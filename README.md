# Fontis
Goal: Use an SQL-comparable language (not necessarily visually similar, but functionally similar) to query data from a configurable, abstracted backend.

E.g. "SELECT * FROM /var/log WHERE extension='.log' AND content LIKE '% 404 %'"
"SELECT * FROM 'http://google.com/#q=foo' WHERE url LIKE '%wikipedia.org%'"

Current implementation is as a basic bash command that allows you to specify other bash commands to run. This is not a fast way to do this, but does generally work.

E.g. select 'wc -w $0' from 'find /home/ubuntu/Documents/' where '[ -f "$0" ]' and '[ -z "$(printf "$0" | grep "/\\.")" ]' and '[ -n "$(printf "$0" | grep "\\.txt\\$")" ]' and '[ -n "$(cat "$0" | grep " !!!")" ]'

Future implementation will probably be in a different language and architected to be both faster and less reliant on utilities like grep, cat, and find.
